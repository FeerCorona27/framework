<?php
    require ("conection.php");
    
    
    
    class model {
        
        public $table;
        public $campos;
        
        public function __construct($tab,$cam) {
            
            $this->campos=$cam;
            $this->table=$tab;
            //echo $this->$table;
            //var_dump($this->campos);
            
        }
        
        
        
        
        public function insert( $cols){
            
            $qry = "INSERT INTO ".$this->table;
            $qry .= " SET ";
            
            
            for ($i=0; $i < count($this->campos); $i++) {
                
                if ($i==0){
                    $qry .= $this->campos[$i]."=".$cols[$i].", ";
                } else if ($i==(count($this->campos)-1)){
                    $qry .= $this->campos[$i]."=\"".$cols[$i]."\" ";
                }
                else {
                    $qry .= $this->campos[$i]."=\"".$cols[$i]."\", ";
                }
            }
            
            
            $qry .= ";";
            echo $qry;
            
            if ($GLOBALS['conn']->query($qry) === TRUE) {
                echo "Record updated successfully";
            } else {
                echo "Error updating record: " .$GLOBALS['conn']->error;
            }
            
            
        }
        
        
        
        
        
        public function select(){  //OK
            
            //var_dump($this->$table);
            $sql = "SELECT * FROM ".$this->table.";";
            echo $sql;
            
            $result = $GLOBALS['conn']->query($sql);
            
            if ($result->num_rows > 0) {
                
                while($row = $result->fetch_assoc()) {
                    echo  $row["id".$this->table]."<br>";
                }
            } else {
                echo "0 results";
            }
            
        }
        
        
        
        
        public function update($condition, $cols){
            
            $qry = "UPDATE ".$this->table."";
            $qry .= " SET ";
            
            
            for ($i=0; $i < count($this->campos); $i++) {
                
                if ($i==0){
                    $qry .= $this->campos[$i]."=".$cols[$i].", ";
                } else if ($i==(count($this->campos)-1)){
                    $qry .= $this->campos[$i]."=\"".$cols[$i]."\" ";
                }
                else {
                    $qry .= $this->campos[$i]."=\"".$cols[$i]."\", ";
                }
            }
            
            
            $qry .= " WHERE ".$condition;
            $qry .= ";";
            echo $qry;
            
            if ($GLOBALS['conn']->query($qry) === TRUE) {
                echo "Record updated successfully";
            } else {
                echo "Error updating record: ".$GLOBALS['conn']->error;
            }
            
        }
        
        
        
        
        public function delete($condition){
            
            $sql = "DELETE FROM ".$this->table." WHERE ".$condition." ;";
            echo $sql;
            if ($GLOBALS['conn']->query($sql) === TRUE) {
                echo "Record deleted successfully";
            } else {
                echo "Error deleting record: " . $conn->error;
            }
        }
        
        
    }
    
    
    
    ?>
